﻿using BDDSample.Persistence;

namespace BDDSample.Test.Unit.Infrastructure
{
    public class UnitTestPersistence
    {
        protected EFDataContext Context { get; }
        protected EFDataContext ReadDataContext { get; }

        public UnitTestPersistence()
        {
            var db = new EFInMemoryDatabase();
            Context = db.CreateDataContext<EFDataContext>();
            ReadDataContext = db.CreateDataContext<EFDataContext>();
        }

        protected void Save<T>(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Context.Manipulate(_ => _.Add(entity));
        }
    }
}
