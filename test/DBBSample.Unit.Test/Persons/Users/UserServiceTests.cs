﻿using BDDSample.Entities.Persons;
using BDDSample.Test.Unit.Infrastructure;
using BDDSample.Tools.Persons.Users;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BDDSample.Test.Unit.Persons.Users
{
    public class UserServiceTests : UnitTestPersistence
    {
        [Fact]
        private async Task Register_add_new_user()
        {
            var role = new Role
            {
                Name = "dammy_name"
            };
            Save(role);
            var dto = UserFactory.CreateAddUserDto(phoneNumber: "dummy_phoneNumber", role.Id);
            var sut = UserFactory.CreateService(Context);

            await sut.Register(dto);

            var expected = await ReadDataContext.Users.Include(_=>_.Role).SingleAsync(); 
            expected.PhoneNumber.Should().Be(dto.PhoneNumber);   
            expected.RoleId.Should().Be(dto.RoleId);
            expected.Role!.Name.Should().Be(role.Name);
        }
    }
}
