﻿using BDDSample.Entities.Persons;
using BDDSample.Persistence.Persons.Users;
using BDDSample.Test.Unit.Infrastructure;
using FluentAssertions;
using Xunit;

namespace BDDSample.Test.Unit.Persons.Users
{
    public class UserQueryTests : UnitTestPersistence
    {
        [Fact]
        private async Task GetUserById_return_a_user_by_id()
        {
            var user = new User
            {
                PhoneNumber = "dummy_number"
            };
            Save(user);
            var sut = new UserQueries(Context);

            var expected = await sut.GetUserById(user.Id);
            expected.Should().NotBeNull();
            expected!.PhoneNumber.Should().Be(user.PhoneNumber);
        }
    }
}
