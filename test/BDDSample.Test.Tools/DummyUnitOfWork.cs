﻿using BDDSample.Persistence;
using BDDSample.Services.Infrastructure.Data;

namespace BDDSample.Tools
{
    public class DummyUnitOfWork : IUnitOfWork
    {
        private readonly EFDataContext context;

        public DummyUnitOfWork(EFDataContext context)
        {
            this.context = context;
        }
        public async Task Begin()
        {

        }

        public async Task Commit()
        {

        }

        public async Task CommitPartial()
        {

        }

        public async Task Complete()
        {
            await context.SaveChangesAsync();
        }

        public async Task Rollback()
        {

        }
    }
}
