﻿using BDDSample.Persistence;
using BDDSample.Persistence.Persons.Users;
using BDDSample.Services.Persons.Users;
using BDDSample.Services.Persons.Users.Contracts;
using BDDSample.Services.Persons.Users.Contracts.Dtos;

namespace BDDSample.Tools.Persons.Users
{
    public static class UserFactory
    {
        public static IUserService CreateService(EFDataContext context)
        {
            var repository = new UserRepository(context);
            var unitOfWork = new EFUnitOfWork(context);
            return new UserService(unitOfWork, repository);
        }

        public static AddUserDto CreateAddUserDto(string phoneNumber, long roleId = 1)
        {
            return new AddUserDto { PhoneNumber = phoneNumber, RoleId = roleId };
        }
    }
}
