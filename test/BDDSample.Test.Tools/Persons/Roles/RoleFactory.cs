﻿using BDDSample.Persistence;
using BDDSample.Persistence.Persons.Roles;
using BDDSample.Services.Persons.Roles;
using BDDSample.Services.Persons.Roles.Contracts;

namespace BDDSample.Tools.Persons.Roles
{
    public static class RoleFactory
    {
        public static IRoleService CreateService(EFDataContext context)
        {
            var repository = new RoleRepository(context);
            var unitOfWork = new EFUnitOfWork(context);
            return new RoleService(unitOfWork, repository);
        }
    }
}
