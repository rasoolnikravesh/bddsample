﻿using Microsoft.Extensions.Configuration;
using Xunit;

namespace BDDSample.Test.Spec.Infrastructure
{
    public class ConfigurationFixture
    {
        private const string PersistenceConfigKey = "PersistenceConfig";
        private const string AppSettingPath = "appsettings.json";
        public PersistenceConfig Value { get; private set; }

        public ConfigurationFixture()
        {
            Value = GetSettings();
        }

        private PersistenceConfig GetSettings()
        {
            var configurations = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(AppSettingPath, optional: true, reloadOnChange: false)
                .AddEnvironmentVariables()
                .AddCommandLine(Environment.GetCommandLineArgs())
                .Build();

            var settings = new PersistenceConfig();
            configurations.Bind(PersistenceConfigKey, settings);
            return settings;
        }
    }

    public class PersistenceConfig
    {
        public string ConnectionString { get; set; } = default!;
    }

    [CollectionDefinition(nameof(ConfigurationFixture), DisableParallelization = true)]
    public class ConfigurationCollectionFixture : ICollectionFixture<ConfigurationFixture>
    {
    }
}
