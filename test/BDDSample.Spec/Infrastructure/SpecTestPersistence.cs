﻿using BDDSample.Persistence;

namespace BDDSample.Test.Spec.Infrastructure
{
    public class SpecTestPersistence : EFDataContextDatabaseFixture
    {
        protected EFDataContext Context { get; }
        protected EFDataContext ReadDataContext { get; }

        public SpecTestPersistence(ConfigurationFixture configuration)
            : base(configuration)
        {
            Context = CreateDataContext();
            ReadDataContext = CreateDataContext();
        }

        protected void Save<T>(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            Context.Add(entity);
            Context.SaveChanges();
        }
    }
}
