﻿using BDDSample.Entities.Persons;
using Microsoft.EntityFrameworkCore;

namespace BDDSample.Persistence
{
    public class EFDataContext : DbContext
    {
        public EFDataContext(string connectionString)
        : this(new DbContextOptionsBuilder<EFDataContext>()
          .UseSqlServer(connectionString).Options)
        {
        }

        private EFDataContext(DbContextOptions<EFDataContext> options) :
            base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EFDataContext).Assembly);
        }

    }
}
