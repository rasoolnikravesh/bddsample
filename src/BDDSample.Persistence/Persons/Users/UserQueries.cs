﻿using BDDSample.Services.Persons.Users.Contracts;
using BDDSample.Services.Persons.Users.Contracts.Queries;
using Microsoft.EntityFrameworkCore;

namespace BDDSample.Persistence.Persons.Users
{
    public class UserQueries : IUserQueries
    {
        private readonly EFDataContext _context;

        public UserQueries(EFDataContext context)
        {
            _context = context;
        }
        public async Task<GetUserDto?> GetUserById(long Id)
        {
            return await _context.Users.Select(_ => new GetUserDto
            {
                Id = _.Id,
                PhoneNumber = _.PhoneNumber
            }).FirstOrDefaultAsync();

        }
    }
}
