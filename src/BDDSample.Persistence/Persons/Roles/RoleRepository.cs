﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Persons.Roles.Contracts;
using Microsoft.EntityFrameworkCore;

namespace BDDSample.Persistence.Persons.Roles
{
    public class RoleRepository : IRoleRepository
    {
        private readonly DbSet<Role> _roles;

        public RoleRepository(EFDataContext context)
        {
            _roles = context.Roles;
        }
       public void Add(Role role)
        {
            _roles.Add(role);   
        }

       public async Task<Role?> FindByName(string roleName)
        {
            return await _roles.FirstOrDefaultAsync(x => x.Name == roleName);    
        }
    }
}
