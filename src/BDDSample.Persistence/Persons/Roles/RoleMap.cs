﻿using BDDSample.Entities.Persons;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BDDSample.Persistence.Persons.Roles
{
    public class RoleMap : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> _)
        {
            _.ToTable("Roles")
                .HasKey(_ => _.Id);
            _.Property(_ => _.Name).HasMaxLength(255);

            _.HasMany(_=>_.Users)
                .WithOne(_=>_.Role)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
