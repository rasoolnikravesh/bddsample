﻿namespace BDDSample.Entities.Persons
{
    public class User
    {
        public User()
        {
            PhoneNumber = string.Empty;
        }
        public long Id { get; set; }
        public string PhoneNumber { get; set; }
        public Role? Role { get; set; }
        public long? RoleId { get; set; }
    }
}