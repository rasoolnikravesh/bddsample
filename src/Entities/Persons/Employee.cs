﻿namespace BDDSample.Entities.Persons
{
    public class Employee
    {
        public Employee()
        {
            FName = string.Empty;
            LName = string.Empty;
            NationalCode = string.Empty;
            FatherName = string.Empty;
        }
        public long Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string NationalCode { get; set; }
        public string FatherName { get; set; }
    }
}
