﻿namespace BDDSample.Entities.Persons
{
    public class Role
    {
        public Role()
        {
            Name = "";
            Users = new HashSet<User>();
        }
        public long Id { get; set; }
        public string Name { get; set; }

        public HashSet<User> Users { get; set; }
    }
}
