﻿namespace BDDSample.Services.Handlers.Persons.Contracts.Dtos
{
    public class AddUserCommand
    {
        public string PhoneNumber { get; set; } = default!;
        public string RoleName { get; set; }
    }
}
