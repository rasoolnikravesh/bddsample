﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Handlers.Persons.Contracts;
using BDDSample.Services.Handlers.Persons.Contracts.Dtos;
using BDDSample.Services.Infrastructure.Data;
using BDDSample.Services.Persons.Roles.Contracts;
using BDDSample.Services.Persons.Roles.Contracts.Dtos;
using BDDSample.Services.Persons.Users.Contracts;
using BDDSample.Services.Persons.Users.Contracts.Dtos;

namespace BDDSample.Services.Handlers.Persons
{
    public class AddUserCommandHandler : UserHandler
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;


        public AddUserCommandHandler(
            IUnitOfWork unitOfWork,
            IUserService userService,
            IRoleService roleService)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
            _roleService = roleService;
        }

        public async Task Command(AddUserCommand command)
        {
            await _unitOfWork.Begin();
            try
            {
                var role = await FindRole(command.RoleName);
                role = await CreateRoleIfNotExist(command.RoleName, role);
                await CreateUser(command.PhoneNumber, role.Id);

                await _unitOfWork.Commit();
            }
            catch
            {
                await _unitOfWork.Rollback();
                throw;
            }
        }

        private async Task CreateUser(string phoneNumber, long roleId)
        {
            await _userService.Register(new AddUserDto
            {
                PhoneNumber = phoneNumber,
                RoleId = roleId
            });
        }

        private async Task<Role?> FindRole(string roleName)
        {
            return await _roleService.FindByName(roleName);
        }

        private async Task<Role> CreateRoleIfNotExist(string roleName, Role? role)
        {
            if (role == null)
            {
                role = await _roleService.Add(new AddRoleDto
                {
                    Name = roleName
                });
            }

            return role;
        }
    }
}
