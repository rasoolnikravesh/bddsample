﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Infrastructure.Data;
using BDDSample.Services.Persons.Users.Contracts;
using BDDSample.Services.Persons.Users.Contracts.Dtos;

namespace BDDSample.Services.Persons.Users
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _repository;

        public UserService(IUnitOfWork unitOfWork, IUserRepository repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public async Task Register(AddUserDto dto)
        {
            var user = new User
            {
                PhoneNumber = dto.PhoneNumber,
                RoleId = dto.RoleId,
            };

            _repository.Add(user);

            await _unitOfWork.Complete();
        }
    }
}
