﻿namespace BDDSample.Services.Persons.Users.Contracts.Dtos
{
    public class AddUserDto
    {
        public string PhoneNumber { get; set; } = default!;
        public long RoleId { get; set; }
    }
}
