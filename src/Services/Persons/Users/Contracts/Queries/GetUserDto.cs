﻿namespace BDDSample.Services.Persons.Users.Contracts.Queries
{
    public class GetUserDto
    {
        public long Id { get; set; }
        public string PhoneNumber { get; set; } = default!;
    }
}
