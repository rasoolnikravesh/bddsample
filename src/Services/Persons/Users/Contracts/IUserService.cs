﻿using BDDSample.Services.Persons.Users.Contracts.Dtos;

namespace BDDSample.Services.Persons.Users.Contracts
{
    public interface IUserService
    {
        Task Register(AddUserDto dto);
    }
}