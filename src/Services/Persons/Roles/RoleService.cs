﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Infrastructure.Data;
using BDDSample.Services.Persons.Roles.Contracts;
using BDDSample.Services.Persons.Roles.Contracts.Dtos;

namespace BDDSample.Services.Persons.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleRepository _reposiotry;

        public RoleService(IUnitOfWork unitOfWork, IRoleRepository reposiotry)
        {
            _unitOfWork = unitOfWork;
            _reposiotry = reposiotry;
        }
        public async Task<Role> Add(AddRoleDto dto)
        {
            var role = new Role
            {
                Name = dto.Name,
            };

            _reposiotry.Add(role);

            await _unitOfWork.Complete();
            return role;
        }

        public async Task<Role?> FindByName(string roleName)
        {
            return await _reposiotry.FindByName(roleName);
        }
    }
}
