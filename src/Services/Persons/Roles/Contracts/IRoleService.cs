﻿using BDDSample.Entities.Persons;
using BDDSample.Services.Persons.Roles.Contracts.Dtos;

namespace BDDSample.Services.Persons.Roles.Contracts
{
    public interface IRoleService
    {
        Task<Role> Add(AddRoleDto dto);
        Task<Role?> FindByName(string roleName);
    }
}
