﻿namespace BDDSample.Services.Persons.Roles.Contracts.Dtos
{
    public class AddRoleDto
    {
        public string Name { get; set; } = default!;
    }
}
