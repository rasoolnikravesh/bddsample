﻿using FluentMigrator;

namespace BDDSample.Migrations
{
    [Migration(202207020856)]
    public class _202207020856_InitialUsers : Migration
    {
        public override void Up()
        {
            Create.Table("Users")
               .WithColumn("Id").AsInt64()
               .Identity().PrimaryKey()
               .WithColumn("PhoneNumber")
               .AsString(20).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("Users");

        }
    }
}
