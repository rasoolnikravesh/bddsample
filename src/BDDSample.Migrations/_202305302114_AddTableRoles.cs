﻿using FluentMigrator;
using System.Data;

namespace BDDSample.Migrations
{
    [Migration(202305302114)]
    public class _202305302114_AddTableRoles : Migration
    {

        public override void Up()
        {
            Create.Table("Roles")
             .WithColumn("Id").AsInt64().NotNullable().Identity().PrimaryKey()
             .WithColumn("Name").AsString(255).NotNullable();

            Create.Column("RoleId").OnTable("Users").AsInt64().NotNullable();

            Create.ForeignKey("FK_Roles_Users").FromTable("Users")
                .ForeignColumn("RoleId").ToTable("Roles")
                .PrimaryColumn("Id").OnDelete(Rule.None);
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_Roles_Users").OnTable("Users");
            Delete.Column("RoleId").FromTable("Users");
            Delete.Table("Roles");
        }
    }
}
